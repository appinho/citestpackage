from lib.my_math import add
import pytest

def test_add():
    assert add(2, 5) == 7
